# portalassapi

Portal Ass Rest API
Esse é o projeto da API do Portal Ass feito em Python com Django Rest Framework.

## Ambiente Desenvolvimento
O ambiente de desenvolvimento foi criado no Docker, então você precisa instalar o Docker e Docker compose.
Feito isso é preciso apenas rodar os seguinte comandos se for a primeira vez que está rodando o projeto.

```bash
docker-compose up --build
```

Fazendo isso você está criando um container com Ubuntu instalando o que é necessário, como Django, Django Rest Framework e outros pacotes necessário. Cria um banco de dados PostgreSQL e usuário e banco necessário.
Por último é criado o banco de dados e as tabelas necessários.
Agora você precisa criar o superuser do Django admin

```bash
docker-compose run api python manage.py createsuperuser
```
