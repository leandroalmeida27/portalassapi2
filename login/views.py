from django.contrib.auth.models import User, Group
from django.http import JsonResponse
from django.contrib.auth import login

from rest_framework import viewsets, exceptions, authentication
from rest_framework.decorators import permission_classes, action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.authtoken.models import Token

from .serializers import UserSerializer


class LoginViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows all login functions
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    parser_classes = [MultiPartParser, FormParser]

    @action(methods=['get'], detail=False, url_path='loggar', url_name='loggar')
    @permission_classes([AllowAny])
    def loggar(self, request):
        if request.method == "GET":

            dataUser = {
                "username" : request.query_params['username'],
                "password" : request.query_params['password'],
            }
            
            try:
                user = authentication.authenticate(username=dataUser['username'], password=dataUser['password'])
                if user is not None:
                    if user.is_active:
                        token, _ = Token.objects.get_or_create(user=user)
                        dataUser = UserSerializer(data=user)
                        login(request, user)
                        dataReturn = {
                            'user': user.username,
                            'token': token.key
                        }
                        return JsonResponse(dataReturn, status=200)
                return JsonResponse({'error': 'Email ou senha incorreto'}, status=400)
            except User.DoesNotExist:
                return JsonResponse({'error': 'Usuário não encontrado no sistema'}, status=400)
    
    @action(methods=['get'], detail=False, url_path='validUser', url_name='validUser')
    @permission_classes([IsAuthenticated])
    def validUser(self, request):
        try:
            userGroups = [getattr(group, 'name') for group in request.user.groups.all()]
            dataReturn = {
                'username': str(request.user),
                'usergroup': userGroups
            }
        except Group.DoesNotExist as e:
            dataReturn = {
                'code': 99,
                'error': e 
            }

        return JsonResponse(dataReturn, status=200)

    @action(methods=['get'], detail=False, url_path='logout', url_name='logout')
    @permission_classes([IsAuthenticated])
    def logout(self, request):
        request.user.auth_token.delete()
        return JsonResponse({'mensagem': 'Logout com sucesso.'}, status=200)

    @action(methods=['post'], detail=False, url_path='cadastrarUser', url_name='cadastrarUser', permission_classes=[AllowAny])
    def cadastrarUser(self, request):

        dataUser = {
                "username" : request.data['mailCadastrar'],
                "email" : request.data['mailCadastrar'],
                "password" : request.data['passwordCadastrar'],
                "first_name" : request.data['nameCadastrar'],
        }

        userSerializer = UserSerializer(data=dataUser)

        if userSerializer.is_valid():
            userCreated = userSerializer.save()
            workerGroup = Group.objects.get(name='worker')
            userCreated.set_password(request.data['passwordCadastrar'])
            userCreated.groups.add(workerGroup)
            userCreated.save()
            

            token, _ = Token.objects.get_or_create(user=userCreated)
            login(request, userCreated)
            dataReturn = {
                'user': userCreated.username,
                'token': token.key
            }
            return JsonResponse(dataReturn, status=200)
        else:
            return JsonResponse({'error': 'Erro ao criar usuário. Tente novamente.'}, status=400)
