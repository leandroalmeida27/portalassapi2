from worker.models import Worker

from rest_framework import serializers

from django.contrib.auth import get_user_model

UserModel = get_user_model()


class UserSerializer(serializers.HyperlinkedModelSerializer, serializers.Serializer):
    class Meta:
        model = UserModel
        fields = [
            "username", "password", "email", "first_name", "groups"
        ]
