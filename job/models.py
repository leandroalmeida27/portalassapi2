from django.db import models
from company.models import Company
from worker.models import Worker

class Job(models.Model):
    company = models.ForeignKey(Company, related_name='company', on_delete=models.CASCADE)
    funcao = models.CharField(max_length=80)
    cidade = models.CharField(max_length=80)
    estado_choice = [
        ("AC", "Acre"),
        ("AL", "Alagoas"),
        ("AP", "Amapá"),
        ("AM", "Amazonas"),
        ("BA", "Bahia"),
        ("CE", "Ceará"),
        ("DF", "Distrito Federal"),
        ("ES", "Espírito Santo"),
        ("GO", "Goiás"),
        ("MA", "Maranhão"),
        ("MT", "Mato Grosso"),
        ("MS", "Mato Grosso do Sul"),
        ("MG", "Minas Gerais"),
        ("PA", "Pará"),
        ("PB", "Paraíba"),
        ("PE", "Pernambuco"),
        ("PI", "Piauí"),
        ("RJ", "Rio de Janeiro"),
        ("RN", "Rio Grande do Norte"),
        ("RS", "Rio Grande do Sul"),
        ("RO", "Rondônia"),
        ("RR", "Roraima"),
        ("SC", "Santa Catarina"),
        ("SP", "São Paulo"),
        ("SE", "Sergipe"),
        ("TO", "Tocantins")
    ]
    estado = models.CharField(
        max_length=2,
        choices=estado_choice
    )
    remuneracao_choice = [
        ("mes", "Por Mês"),
        ("semana", "Por Semana"),
        ("dia", "Por Dia"),
        ("hora", "Por Hora")
    ]
    tipo_remuneracao = models.CharField(
        max_length=6,
        choices=remuneracao_choice
    )
    salario = models.DecimalField(max_digits=12, decimal_places=2)
    quantidade_vaga = models.PositiveSmallIntegerField()
    pcd_choice = [
        (1, "Sim"),
        (2, "Não")
    ]
    vaga_pcd = models.CharField(
        max_length=1,
        choices=pcd_choice
    )
    descricao_vaga = models.TextField()
    vaga_choice = [
        (1, 'Efetivo/CLT'),
        (2, 'Freelancer/MEI'),
        (3, 'Temporário'),
        (4, 'Comissionado'),
        (5, 'Autônomo'),
        (6, 'Aprendiz'),
        (7, 'Estágio'),
        (8, 'Trainee'),
        (9, 'Concurso'),
        (10, 'Voluntário')
    ]
    tipo_vaga = models.CharField(
        max_length=2,
        choices=vaga_choice
    )
    envio_choice = [
        (1, 'Sim'),
        (2, 'Não'),
    ]
    envio_agencia = models.CharField(
        max_length=2,
        choices=envio_choice
    )
    responsavel_vaga = models.CharField(max_length=80)
    confidencial_choice = [
        (1, 'Sim'),
        (2, 'Não'),
    ]
    vaga_confidencial = models.CharField(
        max_length=2,
        choices=confidencial_choice
    )
    email_candidatos_choice = [
        (1, 'Sim'),
        (2, 'Não'),
    ]
    envio_email_candidatos = models.CharField(
        max_length=2,
        choices=email_candidatos_choice
    )
    worker_visualizou = models.PositiveSmallIntegerField(default=0)
    worker_inscrito = models.ManyToManyField(Worker, default=0)
    status_choice = (
        (1, 'Ativo'),
        (2, 'Publicada'),
        (3, 'Inativo')
    )
    status_agencia = models.CharField(
        max_length=2,
        choices=status_choice,
        default=1
    )