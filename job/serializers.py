from rest_framework import serializers

from django.contrib.auth import get_user_model

from job.models import Job

UserModel = get_user_model()

class JobSerializer(serializers.ModelSerializer):
    tipo_vaga_string = serializers.SerializerMethodField()

    class Meta:
        model = Job
        fields = [
            "id", "funcao", "cidade", "estado", "tipo_remuneracao", "salario", "quantidade_vaga", "vaga_pcd", "descricao_vaga", 
            "tipo_vaga", "envio_agencia", "responsavel_vaga", "vaga_confidencial", "envio_email_candidatos", "company", "tipo_vaga_string"
        ]
    
    def create(self, validated_data):
        """
        Create a new job
        """
        return Job.objects.create(**validated_data)

    def get_tipo_vaga_string(self, obj):
        jobIns = Job(obj)
        return jobIns.get_tipo_vaga_display()
