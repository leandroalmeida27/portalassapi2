from django.http import JsonResponse, HttpResponse
from django.core.mail import send_mail
from django.contrib.auth.models import Group
from django.core import serializers
from django.forms.models import model_to_dict

from rest_framework.parsers import JSONParser
from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.decorators import permission_classes, action

from job.serializers import JobSerializer
from job.models import Job
from company.serializers import CompanySerializer
from worker.serializers import UserSerializer
from company.views import CompanyViewSet
from senderemail.views import SenderemailViewSet
from company.models import Company

class JobViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows jobs to be viewed, edited, created or deleted.
    """
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    parser_classes = [MultiPartParser, FormParser]
    permission_classes_by_action = {'create': [AllowAny]}

    @permission_classes([AllowAny])
    def create(self, request, format=None):

        if request.method == "POST":

            data = request.data
            # Data to create the user
            dataUser = {
                "username" : request.data['email'],
                "email" : request.data['email'],
            }

            userSerializer = UserSerializer(data=dataUser)

            if userSerializer.is_valid():
                userCreated = userSerializer.save()
                companyGroup = Group.objects.get(name='company')
                companyGroup.user_set.add(userCreated['user'])
                
                # Add the user ID to create the company
                objCompany = CompanyViewSet()
                dicCompany = objCompany.createDicCompany(data, userCreated['user'].id, userCreated['password'])

                companySerializer = CompanySerializer(data=dicCompany)
                if companySerializer.is_valid():
                    companyCreated = companySerializer.save()
                    dicJob = self.createDicJob(data, companyCreated.id)
                    jobSerializer = JobSerializer(data=dicJob)
                    if jobSerializer.is_valid():
                        jobSerializer.save()
                        self.sendEmailJob(data, userCreated['password'])
                    return JsonResponse(jobSerializer.data, status=200)

                return JsonResponse(companySerializer.errors, status=400)
            else:
                return JsonResponse(userSerializer.errors, status=400)
        return JsonResponse("Problema com a requisição.", status=400)

    def createDicJob(self, data, company, responsavel_vaga=None):
        envio_email_candidatos = 2
        if 'envio_email_candidatos' in data and data['envio_email_candidatos'] == 'on':
            envio_email_candidatos = 1
        dicJob = {
            "funcao": data['funcao'],
            "cidade": data['cidade'],
            "estado": data['estado'],
            "tipo_remuneracao": data['tipo_remuneracao'],
            "salario": data['salario'],
            "quantidade_vaga": data['quantidade_vaga'],
            "vaga_pcd": data['vaga_pcd'],
            "descricao_vaga": data['descricao_vaga'],
            "tipo_vaga": data['tipo_vaga'],
            "envio_agencia": data.get('envio_agencia', 2),
            "responsavel_vaga": data['responsavel_vaga'] if responsavel_vaga == None else responsavel_vaga,
            "vaga_confidencial": data.get('vaga_confidencial', 2), 
            "envio_email_candidatos": envio_email_candidatos,
            "company": company    
        }
        return dicJob

    def sendEmailJob(self, emailData, password):
        emailMessage = (f'Oi {emailData["razao_social"]}'
            f'<br>'
            f'Sua conta foi criada no Portal ASS. Você precisa mudar sua senha no primeiro acesso.<br>'
            f'Usuário: {emailData["email"]} <br>'
            f'Senha: {password}'
        )
        dataToEmail = {
            'subject': "Portal ASS - Conta criada",
            'text': emailMessage,
            'to': [emailData['email']],
            'text_content': 'Sua conta foi criada no Portal ASS. Você precisa mudar sua senha no primeiro acesso. Usuário: ' + emailData["email"] + ' Senha: ' + password
        }
        senderemailViewSet = SenderemailViewSet()
        senderemailViewSet.sendEmail(dataToEmail)

    @action(methods=['get'], detail=False, url_path='getAnunciosVagas', url_name='getAnunciosVagas')
    @permission_classes([IsAuthenticated])
    def getAnunciosVagas(self, request):
        company = Company.objects.get(email=request.user)
        if company:
            jobs = Job.objects.filter(company_id=company.id)
            jobsAnunciados = self.montarDictJobs(jobs)
            return JsonResponse(jobsAnunciados, status=200)
            # return HttpResponse(serializers.serialize('json', jobs), content_type='application/json')
        else:
            return JsonResponse({'error': 'Usuário não encontrado.'}, status=400)

    def montarDictJobs(self, jobs):
        returnDictJobs = {}
        count = 0
        for job in jobs:
            jobInstance = Job(job)
            dictJobs = {
                'cargo': job.funcao,
                'visualizacao': job.worker_visualizou,
                'inscritos': job.worker_inscrito.count(),
                'salario': job.salario,
                'cidade': job.cidade + '-' + job.estado,
                'status': jobInstance.get_status_agencia_display(),
                'pk': job.pk,
                'descricao': job.descricao_vaga
            }
            returnDictJobs[count] = dictJobs
            count += 1

        return returnDictJobs

    def get_permissions(self):
        try:
            # return permission_classes depending on `action` 
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError: 
            # action is not set return default permission_classes
            return [permission() for permission in self.permission_classes]

    @action(methods=['get'], detail=False, url_path='getAnuncio', url_name='getAnuncio')
    @permission_classes([IsAuthenticated])
    def getAnuncio(self, request):
        datapk = request.query_params['pk']
        try:
            job = Job.objects.get(pk=datapk)
            job = model_to_dict(job)
            return JsonResponse(job, status=200)
        except Job.DoesNotExist:
            return JsonResponse({'message': 'Anuncio retirado.'}, status=400)

    @action(methods=['post'], detail=False, url_path='updateVaga', url_name='updateVaga')
    @permission_classes([IsAuthenticated])
    def updateVaga(self, request):
        if request.method == "POST":
            # Data to update the worker
            data = request.data

            email = request.user

            JobData = Job.objects.get(pk=data['vaga_editar'])

            JobData.funcao = data['funcao_editar']
            JobData.cidade = data['cidade_editar']
            JobData.estado = data['estado_editar']
            JobData.tipo_remuneracao = data['tipo_remuneracao_editar']
            JobData.salario = data['salario_editar']
            JobData.quantidade_vaga = data['quantidade_vaga_editar']
            JobData.vaga_pcd = data['vaga_pcd_editar']
            JobData.descricao_vaga = data['descricao_vaga_editar']
            JobData.tipo_vaga = data['tipo_vaga_editar']
            if 'envio_agencia_editar' in data:
                JobData.envio_agencia = data['envio_agencia_editar']
            JobData.save()
            return JsonResponse({'message': 'Dados atualizados com sucesso'}, status=200)

    @action(methods=['post'], detail=False, url_path='cadastraVagaLogado', url_name='cadastraVagaLogado')
    @permission_classes([IsAuthenticated])
    def cadastraVagaLogado(self, request):
        user = request.user
        data = request.data
        try:
            company = Company.objects.get(email=user)
            dicJob = self.createDicJob(request.data, company.id, user.email)
            dicJob['vaga_confidencial'] = 2 if dicJob['vaga_confidencial'] == False else 1
            dicJob['envio_agencia'] = 2 if dicJob['envio_agencia'] == False else 1
            jobSerializer = JobSerializer(data=dicJob)
            if jobSerializer.is_valid():
                jobSerializer.save()
                return JsonResponse(jobSerializer.data, status=201)

        except Company.DoesNotExist:
            return JsonResponse("Empresa não encontrada", status=404)

    @action(methods=['get'], detail=False, url_path='filtro', url_name='filtro')
    @permission_classes([AllowAny])
    def getAnunciosVagasFiltro(self, request):
        filtrosParams = {}
        queryset = Job.objects.all()

        funcao = request.query_params.get('funcao', None)
        cidade = request.query_params.get('cidade', None)
        estado = request.query_params.get('estado', None)
        
        if  funcao:
            filtrosParams['{0}__startswith'.format('funcao')] = funcao

        if cidade:
            filtrosParams['{0}__startswith'.format('cidade')] = cidade
        
        if estado:
            filtrosParams['{0}'.format('estado')] = estado

        if filtrosParams:
            jobs = Job.objects.filter(**filtrosParams)
            jobsAnunciados = self.montarDictJobs(jobs)
            return JsonResponse(jobsAnunciados, status=200)
            # return HttpResponse(serializers.serialize('json', jobs), content_type='application/json')
        else:
            return JsonResponse({'mensagem': 'Nenhuma vaga com os filtros escolhidos.'}, status=204)
