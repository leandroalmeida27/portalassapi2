from worker.models import Worker

from rest_framework import serializers

from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password

UserModel = get_user_model()

class WorkerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Worker
        fields = [
            "nome_completo", "data_nascimento", "cpf", "sexo", "deficiencia", "email", "celular", "telefone", "formacao", 
            "funcao", "pretensao_salarial", "cidade", "estado", "bairro", "user"
        ]
    
    def create(self, validated_data):
        """
        Create a new Worker
        """
        return Worker.objects.create(**validated_data)

class UserSerializer(serializers.HyperlinkedModelSerializer, serializers.Serializer):
    class Meta:
        model = UserModel
        fields = [
            "username", "email"
        ]

    def create(self, validated_data):
        password = UserModel.objects.make_random_password()
        user = UserModel.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            password=make_password(password)
        )
        user.set_password(password)
        dataReturn = {
            'user': user,
            'password': password
        }

        return dataReturn
