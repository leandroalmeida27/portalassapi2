# Generated by Django 3.0.7 on 2020-06-06 06:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('worker', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='worker',
            name='email',
            field=models.EmailField(max_length=80),
        ),
    ]
