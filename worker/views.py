from django.http import JsonResponse, HttpResponse
from django.contrib.auth.models import Group, User
from django.forms.models import model_to_dict
from django.core import serializers

from rest_framework.parsers import JSONParser
from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.decorators import permission_classes, action, authentication_classes

from worker.models import Worker
from worker.serializers import WorkerSerializer, UserSerializer
from curriculum.serializers import CurriculumSerializer, ExperienceSerializer
from senderemail.views import SenderemailViewSet
from curriculum.models import Experience, Curriculum



class WorkerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows worker to be viewed, edited, created or deleted.
    """
    queryset = Worker.objects.all()
    serializer_class = WorkerSerializer
    parser_classes = [MultiPartParser, FormParser]
    permission_classes_by_action = {'create': [AllowAny]}

    @permission_classes([AllowAny])
    @authentication_classes([])
    def create(self, request, format=None):

        if request.method == "POST":
            # Data to create the worker
            data = request.data
            # Data to create the user
            dataUser = {
                "username" : request.data['email'],
                "email" : request.data['email'],
            }
            userSerializer = UserSerializer(data=dataUser)

            if userSerializer.is_valid():
                userCreated = userSerializer.save()
                workerGroup = Group.objects.get(name='worker')
                workerGroup.user_set.add(userCreated['user'])
                workerData = {
                    "nome_completo": data['nome_completo'],
                    "data_nascimento": data['data_nascimento'],
                    "cpf": data['cpf'],
                    "sexo": data['sexo'],
                    "deficiencia": data['deficiencia'],
                    "email": data['email'],
                    "celular": data['celular'],
                    "telefone": data['telefone'],
                    "formacao": data['formacao'],
                    "funcao": data['funcao'],
                    "pretensao_salarial": data['pretensao_salarial'],
                    "cidade": data['cidade'],
                    "estado": data['estado'],
                    "bairro": data['bairro'],
                    "user": userCreated['user'].id,
                    "password": userCreated['password']
                }

                workerSerializer = WorkerSerializer(data=workerData)
                if workerSerializer.is_valid():
                    workerCreated = workerSerializer.save()
                    # Add to data the worker ID to save in curriculum

                    workerData['worker'] = workerCreated.id
                    fileserializer = CurriculumSerializer(data=workerData)
                    if fileserializer.is_valid():
                        fileserializer.save()
                        # TODO: Send email
                        self.sendEmailWorker(workerData)
                        return JsonResponse(workerSerializer.data, status=201)
                    return JsonResponse(fileserializer.data, status=400)
                return JsonResponse(workerSerializer.errors, status=400)
            return JsonResponse(userSerializer.errors, status=400)
        return JsonResponse("Problema com a requisição.", status=400)

    def sendEmailWorker(self, emailData):
        emailMessage = (f'Oi {emailData["nome_completo"]}'
            f'<br>'
            f'Sua conta foi criada no Portal ASS. Você precisa mudar sua senha no primeiro acesso.<br>'
            f'Usuário: {emailData["email"]} <br>'
            f'Senha: {emailData["password"]}'
        )
        dataToEmail = {
            'subject': "Portal ASS - Conta criada",
            'text': emailMessage,
            'to': [emailData['email']],
            'text_content': 'Sua conta foi criada no Portal ASS. Você precisa mudar sua senha no primeiro acesso. Usuário: ' + emailData["email"] + ' Senha: ' + emailData["password"]
        }
        senderemailViewSet = SenderemailViewSet()
        senderemailViewSet.sendEmail(dataToEmail)

    @action(methods=['get'], detail=False, url_path='getWorkerData', url_name='getWorkerData')
    @permission_classes([IsAuthenticated])
    def getWorkerData(self, request):
        email = request.user
        workerData = Worker.objects.get(email=email)
        workerDict = model_to_dict(workerData)
        return JsonResponse(workerDict, status=201)

    def get_permissions(self):
        try:
            # return permission_classes depending on `action` 
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError: 
            # action is not set return default permission_classes
            return [permission() for permission in self.permission_classes]

    @action(methods=['post'], detail=False, url_path='alterar', url_name='alterar')
    @permission_classes([IsAuthenticated])
    def alterarCurriculum(self, request):
        if request.method == "POST":
            # Data to update the worker
            data = request.data

            email = request.user

            workerData = Worker.objects.get(email=email)

            workerData.nome_completo = data['nomeCompleto']
            workerData.data_nascimento = data['dataNascimento']
            workerData.cpf = data['cpf']
            workerData.sexo = data['sexo']
            workerData.deficiencia = data['deficiencia']
            workerData.celular = data['celular']
            workerData.telefone = data['telefone']
            workerData.formacao = data['formacao']
            workerData.funcao = data['funcao']
            workerData.pretensao_salarial = data['pretensao_salarial']
            workerData.cidade = data['cidade']
            workerData.estado = data['estado']
            workerData.bairro = data['bairro']
            workerData.save()
            return JsonResponse({'message': 'Dados atualizados com sucesso'}, status=201)

    @action(methods=['post'], detail=False, url_path='experiencia', url_name='experiencia')
    @permission_classes([IsAuthenticated])
    def cadastrarExperiencia(self, request):
        if request.method == "POST":

            data = {
                'nome_empresa': request.data['nome_empresa'],
                'ramo_empresa': request.data['ramo_empresa'],
                'funcao': request.data['funcao_empresa'],
                'data_entrada': request.data['data_entrada'],
                'data_saida': request.data['data_saida'],
                'atividades': request.data['atividades']
            }
            
            email = request.user
            workerData = Worker.objects.get(email=email)
            data['worker'] = workerData.id
            experienciaSerializer = ExperienceSerializer(data=data)

            if experienciaSerializer.is_valid():
                experienciaSerializer.save()
                print(experienciaSerializer)
                print(experienciaSerializer.data)
                return JsonResponse(experienciaSerializer.data, status=200)
            return JsonResponse(experienciaSerializer.errors, status=400)
        return JsonResponse("Problema com a requisição.", status=400)

    @action(methods=['get'], detail=False, url_path='getExperienciaList', url_name='getExperienciaList')
    @permission_classes([IsAuthenticated])
    def getExperienciaList(self, request):
        userEmail = request.user
        try:
            workerData = Worker.objects.get(email=userEmail)
            experiencia = Experience.objects.filter(worker=workerData.id)
            return HttpResponse(serializers.serialize('json', experiencia), content_type='application/json')
        except Worker.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    @action(methods=['get'], detail=False, url_path='getExperiencia', url_name='getExperiencia')
    @permission_classes([IsAuthenticated])
    def getExperiencia(self, request):
        userEmail = request.user
        try:
            workerData = Worker.objects.get(email=userEmail)
            experiencia = Experience.objects.get(pk=request.query_params['idExperiencia'])
            experiencia = model_to_dict(experiencia)
            return JsonResponse(experiencia, status=200)
        except Worker.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    @action(methods=['post'], detail=False, url_path='alterarExperiencia', url_name='alterarExperiencia')
    @permission_classes([IsAuthenticated])
    def alterarExperiencia(self, request):
        if request.method == "POST":
            # Data to update the worker
            data = request.data

            email = request.user
            try:
                experienciaData = Experience.objects.get(pk=data['experiencia'])
                experienciaData.nome_empresa = data['nome_empresa_alterar']
                experienciaData.ramo_empresa = data['ramo_empresa_alterar']
                experienciaData.funcao = data['funcao_empresa_alterar']
                experienciaData.data_entrada = data['data_entrada_alterar']
                experienciaData.data_saida = data['data_saida_alterar']
                experienciaData.atividades = data['atividades_alterar']
                experienciaData.save()
                return JsonResponse({'message': 'Dados atualizados com sucesso'}, status=200)
            except Experience.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)

    @action(methods=['delete'], detail=False, url_path='deletarExperiencia', url_name='deletarExperiencia')
    @permission_classes([IsAuthenticated])
    def deletarExperiencia(self, request):
        userEmail = request.user
        try:
            workerData = Worker.objects.get(email=userEmail)
            experiencia = Experience.objects.get(pk=request.data['idExperiencia'])
            experiencia.delete()
            return JsonResponse({'mensagem':'Experiencia deletada.'}, status=200)
        except Worker.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)

    @action(methods=['post'], detail=False, url_path='salvaCursosComplementares', url_name='salvaCursosComplementares')
    @permission_classes([IsAuthenticated])
    def salvaCursosComplementares(self, request):
        if request.method == "POST":
            print("Iniciando salvaCursosComplementares")
            data = {
                'cursos_complementares': request.data['cursos_complementares'],
            }
            
            email = request.user
            try:
                workerData = Worker.objects.get(email=email)
                data['worker'] = workerData.id
                try:
                    curriculum = Curriculum.objects.get(worker_id=workerData.id)
                    curriculumSerializer = CurriculumSerializer(curriculum, data=data)
                except Curriculum.DoesNotExist:
                    curriculumSerializer = CurriculumSerializer(data=data)
                    
                print(curriculumSerializer.is_valid())

                if curriculumSerializer.is_valid():
                    curriculumSerializer.save()
                    print(curriculumSerializer.data)
                    return JsonResponse(curriculumSerializer.data, status=200)
                return JsonResponse(curriculumSerializer.errors, status=400)
            except Worker.DoesNotExist:
                return JsonResponse({'message':'Usuário não exite'}, status=400)
        return JsonResponse("Problema com a requisição.", status=400)

    @action(methods=['get'], detail=False, url_path='getCursosComplementares', url_name='getCursosComplementares')
    @permission_classes([IsAuthenticated])
    def getCursosComplementares(self, request):
        userEmail = request.user
        try:
            workerData = Worker.objects.get(email=userEmail)
            print(workerData.id)
            try:
                curriculum = Curriculum.objects.get(worker_id=workerData.id)
                curriculum = model_to_dict(curriculum)
            except Curriculum.DoesNotExist:
                return JsonResponse({'mensagem': 'Trabalhador não tem cursos complementares'}, status=200)
            
            print(curriculum)
            return JsonResponse(curriculum, status=200)
        except Worker.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
