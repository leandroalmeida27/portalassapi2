from django.db import models
from django.contrib.auth.models import User

class Worker(models.Model):
    user = models.ForeignKey(User, default=3, on_delete=models.CASCADE)
    nome_completo = models.CharField(max_length=80)
    data_nascimento = models.DateField()
    cpf = models.CharField(max_length=11)
    sexo_choice = [
        ("F", "Feminino"),
        ("M", "Masculino"),
    ]
    sexo = models.CharField(
        max_length=1,
        choices=sexo_choice
    )
    deficiencia_choice = [
        (1, "Sim"),
        (2, "Não")
    ]
    deficiencia = models.CharField(
        max_length = 1,
        choices = deficiencia_choice
    )
    email = models.EmailField(max_length=80)
    celular = models.CharField(max_length=12)
    telefone = models.CharField(max_length=12)
    formacao_choice = [
        (1, "Ensino Fundamental Incompleto"),
        (2, "Ensino Fundamental Completo"),
        (3, "Ensino Médio Incompleto"),
        (4, "Ensino Médio Completo"),
        (5, "Técnico/Pós-Médio Incompleto"),
        (6, "Técnico/Pós-Médio Completo"),
        (7, "Tecnólogo Incompleto"),
        (8, "Superior Incompleto"),
        (9, "Tecnólogo Completo"),
        (10, "Superior Completo")
    ]
    formacao = models.CharField(
        max_length=2,
        choices=formacao_choice
    )
    funcao = models.CharField(max_length=80)
    pretensao_salarial = models.DecimalField(max_digits=20, decimal_places=2)
    cidade = models.CharField(max_length=80)
    cep = models.CharField(max_length=8, default='0')
    estado_choice = [
        ("AC", "Acre"),
        ("AL", "Alagoas"),
        ("AP", "Amapá"),
        ("AM", "Amazonas"),
        ("BA", "Bahia"),
        ("CE", "Ceará"),
        ("DF", "Distrito Federal"),
        ("ES", "Espírito Santo"),
        ("GO", "Goiás"),
        ("MA", "Maranhão"),
        ("MT", "Mato Grosso"),
        ("MS", "Mato Grosso do Sul"),
        ("MG", "Minas Gerais"),
        ("PA", "Pará"),
        ("PB", "Paraíba"),
        ("PE", "Pernambuco"),
        ("PI", "Piauí"),
        ("RJ", "Rio de Janeiro"),
        ("RN", "Rio Grande do Norte"),
        ("RS", "Rio Grande do Sul"),
        ("RO", "Rondônia"),
        ("RR", "Roraima"),
        ("SC", "Santa Catarina"),
        ("SP", "São Paulo"),
        ("SE", "Sergipe"),
        ("TO", "Tocantins")
    ]
    estado = models.CharField(
        max_length=2,
        choices=estado_choice
    )
    bairro = models.CharField(max_length=80)
