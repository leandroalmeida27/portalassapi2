from company.models import Company
from rest_framework import viewsets
from company.serializers import CompanySerializer

class CompanyViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows company to be viewed or edited.
    """
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    def createDicCompany(self, data, userid, password):
        dicCompany = {
            'razao_social': data['razao_social'], 
            'tamanho_empresa': data['tamanho_empresa'],
            'cnpj': data['cnpj'],
            'telefone': data['telefone'],
            'email': data['email'],
            'cidade_company': data['cidade_company'],
            'estado_company': data['estado_company'], 
            'bairro_company': data['bairro_company'], 
            'user': userid,
            'password': password
        }

        return dicCompany

