from company.models import Company
from rest_framework import serializers, viewsets

# Serializers define the API representation.
class CompanySerializer(serializers.ModelSerializer):
    # incoming_data = serializers.JSONField()
    # 'incoming_data'
    class Meta:
        model = Company
        fields = [
            'razao_social', 'tamanho_empresa', 'cnpj', 'telefone', 'email', 'cidade_company', 'estado_company', 'bairro_company',
            'user'
        ]

    def create(self, validated_data):
        """
        Create a new Worker
        """
        return Company.objects.create(**validated_data)