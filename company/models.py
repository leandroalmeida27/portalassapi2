from django.db import models
from django.contrib.auth.models import User

class Company(models.Model):
    user = models.ForeignKey(User, default=3, on_delete=models.CASCADE)
    razao_social = models.CharField(max_length=80)
    tamanho_empresa_escolhas = [
        (1, 'Pequeno Porte'),
        (2, 'Médio Porte'),
        (3, 'Grande Porte'),
    ]
    tamanho_empresa = models.CharField(
        max_length=1,
        choices=tamanho_empresa_escolhas,
    )
    cnpj = models.CharField(max_length=14)
    telefone = models.CharField(max_length=12)
    email = models.EmailField(max_length=80)
    cidade_company = models.CharField(max_length=80, default='Não Fornecido')
    estado_choice = [
        ("AC", "Acre"),
        ("AL", "Alagoas"),
        ("AP", "Amapá"),
        ("AM", "Amazonas"),
        ("BA", "Bahia"),
        ("CE", "Ceará"),
        ("DF", "Distrito Federal"),
        ("ES", "Espírito Santo"),
        ("GO", "Goiás"),
        ("MA", "Maranhão"),
        ("MT", "Mato Grosso"),
        ("MS", "Mato Grosso do Sul"),
        ("MG", "Minas Gerais"),
        ("PA", "Pará"),
        ("PB", "Paraíba"),
        ("PE", "Pernambuco"),
        ("PI", "Piauí"),
        ("RJ", "Rio de Janeiro"),
        ("RN", "Rio Grande do Norte"),
        ("RS", "Rio Grande do Sul"),
        ("RO", "Rondônia"),
        ("RR", "Roraima"),
        ("SC", "Santa Catarina"),
        ("SP", "São Paulo"),
        ("SE", "Sergipe"),
        ("TO", "Tocantins")
    ]
    estado_company = models.CharField(
        max_length=2,
        choices=estado_choice,
        default="AC"
    )
    bairro_company = models.CharField(max_length=80, default='Não Fornecido')
