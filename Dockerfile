FROM ubuntu:18.04

RUN apt update
RUN apt-get update && apt-get upgrade -y && apt-get clean && apt-get install -y libpq-dev gcc
RUN apt-get install -y curl python3.7 python3.7-dev python3.7-distutils python3-pip

# Register the version in alternatives
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.7 1

# Set python 3 as the default python
RUN update-alternatives --set python /usr/bin/python3.7

# Upgrade pip to latest version
RUN curl -s https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
    python get-pip.py --force-reinstall && \
    rm get-pip.py

# RUN pip install django
# RUN pip install djangorestframework
# RUN pip install psycopg2

# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

# create root directory for our project in the container
RUN mkdir /portalassApi

# Set the working directory to /music_service
WORKDIR /portalassApi

# Copy the current directory contents into the container at /music_service
ADD . /portalassApi/

RUN pip install --user -r requirements.txt
