from django.db import models

class Foto(models.Model):
    titulo = models.CharField(max_length=120)
    data_upload = models.DateTimeField(auto_now_add=True)
    filename = models.CharField(max_length=120)
    # arquivo = models.ImageField(upload_to='images')

    def __str__(self):
        self.titulo

    class Meta:
        db_table = ''
        managed = True
        verbose_name = 'Foto'
        verbose_name_plural = 'Fotos'

class Album(models.Model):
    titulo = models.CharField(max_length=250)
    foto = models.ManyToManyField(Foto, blank=True)

    def __str__(self):
        self.titulo

    class Meta:
        managed = True
        verbose_name = 'Album'
        verbose_name_plural = 'Albums'

