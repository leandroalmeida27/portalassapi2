from django.db import models
from worker.models import Worker

class Curriculum(models.Model):
    # cv_file = models.FileField(max_length=None, upload_to='file')
    worker = models.ForeignKey(Worker, related_name='worker_id', on_delete=models.CASCADE)
    observacao = models.TextField(null=True, blank=True)
    cursos_complementares = models.TextField(null=True, blank=True)


class Experience(models.Model):
    worker = models.ForeignKey(Worker, related_name='worker_experience', on_delete=models.CASCADE, default=0)
    nome_empresa = models.CharField(max_length=80)
    ramo_empresa = models.CharField(max_length=80)
    funcao = models.CharField(max_length=80)
    data_entrada = models.DateField()
    data_saida = models.DateField()
    atividades = models.TextField()