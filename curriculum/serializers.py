from rest_framework import serializers, viewsets

from curriculum.models import Curriculum, Experience

class CurriculumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Curriculum
        fields = [ "worker", "observacao", "cursos_complementares" ]

class ExperienceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experience
        fields = ["worker", "nome_empresa", "ramo_empresa", "funcao", "data_entrada", "data_saida", "atividades"]