from django.core.mail import send_mail, BadHeaderError, EmailMultiAlternatives
from django.conf import settings

# from rest_framework import viewsets

class SenderemailViewSet:

    def sendEmail(self, dataEmail):
        if ('subject', 'text', 'to') not in dataEmail:
            dataReturn = {
                'code': 99,
                'message': 'Um dos campos subject, text, to - está faltando no parametro'
            }

        try:
            msg = EmailMultiAlternatives(dataEmail['subject'], dataEmail['text_content'], settings.EMAIL_FROM, dataEmail['to'])
            msg.attach_alternative(dataEmail['text'], "text/html")
            msg.send()

            dataReturn = {
                'code' : 200,
                'message': 'E-mail enviado com sucesso'
            }
        except BadHeaderError:
            dataReturn = {
                'code': 99,
                'message': 'Problema no envio do e-mail',
                'raise': 'Invalid header found.'
            }
        return dataReturn
