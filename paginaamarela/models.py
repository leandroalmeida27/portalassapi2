from django.db import models


class Categoria(models.Model):
    categoria = models.CharField(max_length=100, blank=True, null=True)
    
    def __str__(self):
        return self.categoria

class Paginaamarela(models.Model):
    nomeEmpresa = models.CharField(max_length=80)
    email = models.CharField(max_length=120)
    telefone = models.CharField(max_length=12)
    site = models.URLField(max_length=250, blank=True, null=True)
    cep = models.CharField(max_length=8, default='0')
    logradouro = models.CharField(max_length=200, blank=True, null=True)
    numero = models.CharField(max_length=6, blank=True, null=True)
    estado_choice = [
        ("AC", "Acre"),
        ("AL", "Alagoas"),
        ("AP", "Amapá"),
        ("AM", "Amazonas"),
        ("BA", "Bahia"),
        ("CE", "Ceará"),
        ("DF", "Distrito Federal"),
        ("ES", "Espírito Santo"),
        ("GO", "Goiás"),
        ("MA", "Maranhão"),
        ("MT", "Mato Grosso"),
        ("MS", "Mato Grosso do Sul"),
        ("MG", "Minas Gerais"),
        ("PA", "Pará"),
        ("PB", "Paraíba"),
        ("PE", "Pernambuco"),
        ("PI", "Piauí"),
        ("RJ", "Rio de Janeiro"),
        ("RN", "Rio Grande do Norte"),
        ("RS", "Rio Grande do Sul"),
        ("RO", "Rondônia"),
        ("RR", "Roraima"),
        ("SC", "Santa Catarina"),
        ("SP", "São Paulo"),
        ("SE", "Sergipe"),
        ("TO", "Tocantins")
    ]
    estado = models.CharField(
        max_length=2,
        choices=estado_choice,
        blank=True, null=True
    )
    bairro = models.CharField(max_length=80, blank=True, null=True)
    cidade = models.CharField(max_length=80, blank=True, null=True)
    complemento = models.CharField(max_length=100, blank=True, null=True)
    facebook = models.CharField(max_length=250, blank=True, null=True)
    instagram = models.CharField(max_length=250, blank=True, null=True)
    twitter = models.CharField(max_length=250, blank=True, null=True)
    atividades = models.TextField(null=True, blank=True)
    imagemEmpresa = models.ImageField(upload_to='images/paginaamarela/', default=0)
    categoria = models.ManyToManyField(Categoria, blank=True)

    class Meta:
        ordering = ['-pk']
