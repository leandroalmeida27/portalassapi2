from django.contrib import admin

from .models import Paginaamarela, Categoria

admin.site.register(Paginaamarela)
admin.site.register(Categoria)
