from paginaamarela.models import Paginaamarela, Categoria

from rest_framework import serializers

class CategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categoria
        fields = [
            'id', 'categoria'
        ]

class PaginaamarelaSerializer(serializers.ModelSerializer):
    categoria = CategoriaSerializer(many=True, read_only=True)

    class Meta:
        model = Paginaamarela
        fields = [
            'id', "nomeEmpresa", "email", "telefone", "site", "cep", "logradouro", "numero", "estado", "bairro", 
            "cidade", "complemento", "facebook", "instagram", "twitter", "atividades", "imagemEmpresa", "categoria"
        ]

    # def create(self, validated_data):
    #     categorias = validated_data.pop('categoria')
    #     paginaAmarela = Paginaamarela.objects.create(**validated_data)
    #     for categoria in categorias:
    #         Categoria.objects.create(paginaamarela = paginaAmarela, **categoria)
    #     return paginaAmarela
