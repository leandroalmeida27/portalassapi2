from django.http import JsonResponse

from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.decorators import action
from rest_framework.response import Response

from .serializers import PaginaamarelaSerializer, CategoriaSerializer
from .models import Paginaamarela, Categoria


class CategoriaViewSet(viewsets.ModelViewSet):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer
    permission_classes = [AllowAny]

class PaginaamarelaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows pagina amarela to be viewed or edited.
    """
    queryset = Paginaamarela.objects.all()
    serializer_class = PaginaamarelaSerializer
    permission_classes = [AllowAny]
    
    def create(self, request):
        paginaamarela = PaginaamarelaSerializer(data=request.data)
        if paginaamarela.is_valid():
            new_paginaamarela = paginaamarela.save()
            categoria = Categoria.objects.get(pk=request.data['categoria'])
            new_paginaamarela.categoria.set(request.data['categoria'])
            new_paginaamarela.save()
            return JsonResponse({'message': 'Cadastrado com sucesso.'}, status=200)
    
    @action(methods=['get'], detail=False, url_path='getPaginaByCategoria', url_name='getPaginaByCategoria')
    def getPaginaByCategoria(self, request):
        print(request.query_params.get('categoria'))
        paginasAmarelas = Paginaamarela.objects.filter(categoria=request.query_params.get('categoria'))
        resposta = PaginaamarelaSerializer(paginasAmarelas, many=True)
        return Response(resposta.data, status=200)

    @action(methods=['get'], detail=False, url_path='getPaginaById', url_name='getPaginaById')
    def getPaginaById(self, request):
        print(request.query_params.get('pk'))
        paginaDetalhe = Paginaamarela.objects.filter(pk=request.query_params.get('pk'))
        resposta = PaginaamarelaSerializer(paginaDetalhe, many=True)
        return Response(resposta.data, status=200)
