# Generated by Django 3.0.8 on 2020-08-16 01:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Faleconosco',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome_completo', models.CharField(max_length=80)),
                ('cpf', models.CharField(max_length=11)),
                ('email', models.EmailField(max_length=80)),
                ('perfil', models.CharField(choices=[(1, 'Trabalhador'), (2, 'Empregador')], max_length=1)),
                ('assunto', models.CharField(choices=[(1, 'Dúvidas'), (2, 'Reclamações'), (3, 'Elogios'), (4, 'Outros')], max_length=1)),
                ('mensagem', models.TextField()),
            ],
        ),
    ]
