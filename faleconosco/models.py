from django.db import models

class Faleconosco(models.Model):
    nome_completo = models.CharField(max_length=80)
    cpf = models.CharField(max_length=11)
    email = models.EmailField(max_length=80)
    perfil_choice = [
        (1, "Trabalhador"),
        (2, "Empregador"),
    ]
    perfil = models.CharField(
        max_length=1,
        choices=perfil_choice
    )
    assunto_choice = [
        (1, "Dúvidas"),
        (2, "Reclamações"),
        (3, "Elogios"),
        (4, "Outros")
    ]
    assunto = models.CharField(
        max_length=1,
        choices=assunto_choice
    )
    mensagem = models.TextField()
