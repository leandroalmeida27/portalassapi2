from rest_framework import serializers, viewsets

from .models import Faleconosco
from senderemail.views import SenderemailViewSet
from portalassapi import settings

# Serializers define the API representation.
class FaleconoscoSerializer(serializers.ModelSerializer):
    # perfil_type_name = serializers.SerializerMethodField()
    # def get_perfil_type_name(self, obj):
    #     return obj.get_perfil_type_display()
    # # perfil = serializers.ChoiceField(choices=Faleconosco.perfil_choice)
    assunto = serializers.ChoiceField(choices=Faleconosco.assunto_choice)

    class Meta:
        model = Faleconosco
        fields = [
            'nome_completo', 'cpf', 'email', 'perfil', 'assunto', 'mensagem'
        ]

    def create(self, validated_data):
        """
        Create a new Fale Conosco
        """
        faleconosco = Faleconosco.objects.create(**validated_data)
        emailMessage = (f'Email do Fale Conosco'
            f'<br>'
            f'Nome Completo: {faleconosco.nome_completo}<br>'
            f'CPF: {faleconosco.cpf} <br>'
            f'email: {faleconosco.email} <br>'
            f'perfil: {faleconosco.get_perfil_display()} <br>'
            f'mensagem: {faleconosco.mensagem}' 
        )

        dataToEmail = {
            'subject': "{0}".format(faleconosco.get_assunto_display()),
            'text': emailMessage,
            'to': settings.EMAIL_TO,
            'text_content': ''
        }
        senderemailViewSet = SenderemailViewSet()
        senderemailViewSet.sendEmail(dataToEmail)
        return faleconosco