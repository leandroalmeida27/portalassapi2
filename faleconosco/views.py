from rest_framework import viewsets
from rest_framework.decorators import permission_classes
from rest_framework.permissions import AllowAny

from .serializers import FaleconoscoSerializer
from .models import Faleconosco

class FaleconoscoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Fale Conosco be saved.
    """
    queryset = Faleconosco.objects.all()
    serializer_class = FaleconoscoSerializer
    permission_classes_by_action = {'create': [AllowAny]}


    def get_permissions(self):
        try:
            # return permission_classes depending on `action` 
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError: 
            # action is not set return default permission_classes
            return [permission() for permission in self.permission_classes]
