"""portalassapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from rest_framework import routers

from company import views as viewsCompany
from worker import views as viewsWorker
from job import views as viewsJob
from login import views as viewsLogin
from faleconosco import views as viewsFaleconosco
from paginaamarela import views as viewsPaginaamarela

router = routers.DefaultRouter()
router.register(r'company', viewsCompany.CompanyViewSet)
router.register('worker', viewsWorker.WorkerViewSet)
router.register('job', viewsJob.JobViewSet)
router.register('login', viewsLogin.LoginViewSet)
router.register('faleconosco', viewsFaleconosco.FaleconoscoViewSet)
router.register('paginaamarela', viewsPaginaamarela.PaginaamarelaViewSet)
router.register('categorias', viewsPaginaamarela.CategoriaViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path(r'api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('accounts/', include('rest_registration.api.urls'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
